{ pkgs ? import ./default.nix }:
pkgs.stdenv.mkDerivation {
  name = "forth-example";
  buildInputs = [ pkgs.gforth pkgs.theForthNet.fixed pkgs.theForthNet.gencon pkgs.theForthNet.CRC-8 pkgs.theForthNet.ll ];
}
