addForthPackagePath() {
	addToSearchPathWithCustomDelimiter : FORTH_PACKAGE_PATH $1/share/common-forth-packages
}

addEnvHooks "$hostOffset" addForthPackagePath

copyForthPackagesToBuildHook() {
	if [ "$copyForthPackagesToBuild" -gt 0 ]; then
		echo initializing forth-packages
		( IFS=:
		  for d in $FORTH_PACKAGE_PATH;
		  do
			  echo "directory $d"
			  mkdir -p ./forth-packages
			  cp -r -t ./forth-packages $d/forth-packages/*
		  done )
	fi
}

preBuildHooks+=(copyForthPackagesToBuildHook)
