{ buildForthNetPackage }: {
  base64 = buildForthNetPackage {
    pname = "base64";
    version = "1.0.0";
    hash = "sha256-ZKEF19tDrCptBDPWWps4nubVkgA7Jx7qtAxONazsgTQ=";
  };
  bit-arrays = buildForthNetPackage {
    pname = "bit-arrays";
    version = "3.0.2";
    hash = "sha256-Dcgv3qckyo3WGsBCHCwE80WES0UnP4tfhu2pxOlhWwY=";
  };
  bounds = buildForthNetPackage {
    pname = "bounds";
    version = "1.0.0";
    hash = "sha256-nIBHX8fh2atUn/S3r71Y++ZZkSGSzbvMSGYBEhgLV4c=";
  };
  break = buildForthNetPackage {
    pname = "break";
    version = "1.0.0";
    hash = "sha256-KaSmh4jJBIjqQGXahTAXGl4DZ5H0odZ0Q5QyI6Hf3bA=";
  };
  callec = buildForthNetPackage {
    pname = "callec";
    version = "0.0.2";
    hash = "sha256-KrdK0VBSkfJAtRm83SlBG2GIzHScdaKsaDuCjZYGz7Y=";
  };
  co = buildForthNetPackage {
    pname = "co";
    version = "0.0.1";
    hash = "sha256-K5t00DeRuxVFmSFvZs93QbCbKIv1P1ueRJg+q3/8jwo=";
  };
  compat = buildForthNetPackage {
    pname = "compat";
    version = "1.1.0";
    hash = "sha256-CBF/2++zU6g0mEEoMK1IeNMBo8wC650C9UcKTAb8IAE=";
  };
  CRC-8 = buildForthNetPackage {
    pname = "CRC-8";
    version = "0.1.0";
    hash = "sha256-kWMUjatJdhA+HCjWvbZxqtibYq7jm1BrKRhUiB6Mtxk=";
  };
  dynamic-memory-allocation = buildForthNetPackage {
    pname = "dynamic-memory-allocation";
    version = "1.0.2";
    hash = "sha256-iw1dFelVf4PU1eREAuaDRjW2TawHe0lBrZhBwd23F/A=";
  };
  euler303 = buildForthNetPackage {
    pname = "euler303";
    version = "1.0.0";
    hash = "sha256-n7xSZQi9f33XGLOOxS4/QpahQUanPjbr503jr+lhLzA=";
  };
  f = buildForthNetPackage {
    pname = "f";
    version = "0.2.4";
    hash = "sha256-MTucveP9fB+07IWZLVTzIk4YSlL4LK/7qc5EDC43pvU=";
  };
  fixed = buildForthNetPackage {
    pname = "fixed";
    version = "1.0.0";
    hash = "sha256-iyKwXRqb0BkjW8X1GA0zdX6p0AeCrFfLFZ2J5On5Lvw=";
  };
  forthvector = buildForthNetPackage {
    pname = "forthvector";
    version = "0.0.1";
    hash = "sha256-paFRsYnm23dPFVaxkuVQyzLYGIeRcoAkQOxyMfmgTvk=";
  };
  gencon = buildForthNetPackage {
    pname = "gencon";
    version = "0.1.0";
    hash = "sha256-BXGYBHwgKtSBUQz2QY1veznjZdzTiHmWcc23UrtFGlo=";
  };
  i2c = buildForthNetPackage {
    pname = "i2c";
    version = "1.0.3";
    hash = "sha256-Cw35z8faQFnfSMbEo5JebZui5xopzQz3vX9F3pcSieo=";
  };
  interpretive = buildForthNetPackage {
    pname = "interpretive";
    version = "1.0.2";
    hash = "sha256-JuMKl4kQRs6ksMQ3ek1C/TtjkmmL99Z42+Aib2+XH4s=";
  };
  keccak = buildForthNetPackage {
    pname = "keccak";
    version = "1.1.2";
    hash = "sha256-Qba/20maRl1okHwGdfARdwQ5wtVpGhN1Bi/2rWf3K9k=";
  };
  lcd-hd44780 = buildForthNetPackage {
    pname = "lcd-hd44780";
    version = "0.1.0";
    hash = "sha256-AHVNHj28HNl9pryUR43+OP3qOfMYsoel0dvFTm7D6Mo=";
  };
  ll = buildForthNetPackage {
    pname = "ll";
    version = "1.0.2";
    hash = "sha256-KANO5e1C4UhbzkXYstLimX6EN/kq11NUuzjBpG2N6ig=";
  };
  matmul = buildForthNetPackage {
    pname = "matmul";
    version = "2.0.2";
    hash = "sha256-f2StzotDr5MCoSGo+ubNWjLudq9P+0OL79X6oO9qDvM=";
  };
  minimal = buildForthNetPackage {
    pname = "minimal";
    version = "1.1.1";
    hash = "sha256-mqaPaRQM8HeMpN+ovpOfO2TRbWORgFQSAI2YP1KL7Zg=";
  };
  modules = buildForthNetPackage {
    pname = "modules";
    version = "1.0.2";
    hash = "sha256-+VtKAEzUYqBd0NVnTGG9VtkeQ5fqJloKuyAlTDLn64s=";
  };
  mrot = buildForthNetPackage {
    pname = "mrot";
    version = "1.0.0";
    hash = "sha256-V0mHIsbSe5CImTYuQ2A9qop6ItjBr5B1pES1Fnj/aQo=";
  };
  multi-tasking = buildForthNetPackage {
    pname = "multi-tasking";
    version = "0.4.0";
    hash = "sha256-Dnbzaap+5R7yb6Z3Qo6KzJbux125Vfl4VsfYZH3KOrA=";
  };
  mwords = buildForthNetPackage {
    pname = "mwords";
    version = "1.0.1";
    hash = "sha256-C4Cz2yv9BoY4h/IF67Y2UHLAPCTUPuPrsq75pu6vI0I=";
  };
  nige-zapper = buildForthNetPackage {
    pname = "nige-zapper";
    version = "0.1.0";
    hash = "sha256-f2L/pFBeArVNeOWum7+f9YAVBGeMRROZiZRXcdqueEA=";
  };
  objects = buildForthNetPackage {
    pname = "objects";
    version = "1.1.1";
    hash = "sha256-rWm7YRXDEXwPbMbiTmHxyYTfxw0hEqq5ZAoITkynUek=";
  };
  package = buildForthNetPackage {
    pname = "package";
    version = "0.1.2";
    hash = "sha256-pAsY16PRFHTBjeYibrkKNdjxsViUIJ+8NX8tociO2Ug=";
  };
  priority-queue = buildForthNetPackage {
    pname = "priority-queue";
    version = "1.0.0";
    hash = "sha256-jdLMhJwVJ74MKZwgy1LuCCHCnzJxF8iwL8uTAles4B0=";
  };
  recognizers = buildForthNetPackage {
    pname = "recognizers";
    version = "2.1.0";
    hash = "sha256-ft8+NHO7Bs4Pu13v5CXPDBPXatFI4owwQjkJXwIJqFc=";
  };
  screens = buildForthNetPackage {
    pname = "screens";
    version = "1.0.0";
    hash = "sha256-X7i+gjaM9UZOM8JsQyh7q1rw2W4LtlH5X/Wr/hPFsnI=";
  };
  sfp = buildForthNetPackage {
    pname = "sfp";
    version = "1.0.0";
    hash = "sha256-lM5EuwG2BDVRlBdXHhrenPc01Hr6HzdGFzEoVgZh3j4=";
  };
  sprintf = buildForthNetPackage {
    pname = "sprintf";
    version = "1.0.2";
    hash = "sha256-TGzVxcQtBhdhA4U5P7c2d4yADu47rAW/Mcinwh14awU=";
  };
  stack = buildForthNetPackage {
    pname = "stack";
    version = "1.0.0";
    hash = "sha256-bj9z5JEC2zwGXeXfS4fdmEecufVdNf14/hArqD8NcOk=";
  };
  stringstack = buildForthNetPackage {
    pname = "stringstack";
    version = "1.0.3";
    hash = "sha256-3BtsIu5RNz6h9vxJ/RjKhJYGQ5T+QYhxoxKEhfJQ2Ek=";
  };
  swoop-compat = buildForthNetPackage {
    pname = "swoop-compat";
    version = "0.9.1";
    hash = "sha256-A2IS/+zu6WZ22XnWgcUJK6fXZju6qkKY09b2o+7lsP0=";
  };
  tail-jump = buildForthNetPackage {
    pname = "tail-jump";
    version = "0.0.1";
    hash = "sha256-GFlo0s9KyLrCTNeIyqFnQFqcITcMrcHf/bI+LGrZTdI=";
  };
  ttester = buildForthNetPackage {
    pname = "ttester";
    version = "1.1.0";
    hash = "sha256-MJqgL4IAcDn+G35W1p3YbhNt0q7icmxFTW25Xa8f/94=";
  };
}
