require forth-packages/ll/x.x.x/ll.fs

variable list1

create elem-tail list1 ll, 10 ,
create elem-mid list1 ll, 20 ,
create elem-head list1 ll, 30 ,

T{ list1 @ ll-length -> 3 }T
T{ list1 @ -> elem-head }T
T{ list1 @ @ -> elem-mid }T
T{ list1 @ @ @ -> elem-tail }T
T{ list1 1 ll-index cell+ @ -> 30 }T



