final: prev: let
  versions = prev.lib.versions;
  forthCommon = prev.stdenv.mkDerivation {
    name = "forth-common";
    src = builtins.filterSource (path: type: false) ./.;
    setupHook = ./common-hook.sh;
  };
in rec {
  gforth = prev.gforth.overrideAttrs ({version, ...}: {
    sitePackages = "share/gforth/${version}";
    setupHook = ./gforth-hook.sh;
  });
  buildForthNetPackage = { pname, version, hash }: prev.stdenv.mkDerivation {
    pname = pname;
    version = version;
    propagatedBuildInputs = [ forthCommon ];
    buildInputs = [ forthCommon ];
    src = prev.fetchzip {
      url = "http://theforth.net/package/${pname}/${version}.zip";
      hash = hash;
    };
    installPhase = ''
runHook preInstall
mkdir -p $out/${gforth.sitePackages}/forth-packages/$pname/
packageDir=$out/${gforth.sitePackages}/forth-packages/$pname/
cp -r $src/$version $packageDir/$version
ln -s $version $packageDir/${versions.major version}.x.x
ln -s $version $packageDir/${versions.majorMinor version}.x
ln -s $version $packageDir/x.x.x
# we use this long path name because we don't want conflict with forth-specific files, project-specific files, user-specific files, or site-specific files
ln -s $out/${gforth.sitePackages} $out/share/common-forth-packages
runHook postInstall
'';
  };
  theForthNet = import ./index.nix { inherit buildForthNetPackage; };
}
