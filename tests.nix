{ pkgs ? import <nixpkgs> { overlays = [ (import ./overlay.nix) ]; }
, stdenv ? pkgs.stdenv
, mkDerivation ? stdenv.mkDerivation
, gforth ? pkgs.gforth
}:
{
  tests = {
    gforth.ll = mkDerivation {
      name = "theForthNet-ll-tests-gforth";
      src = ./tests/ll.fs;
      phases = [ "buildPhase" ];
      buildInputs = [ gforth pkgs.theForthNet.ll ];
      buildPhase = ''
set -e
echo GFORTHPATH is $GFORTHPATH
gforth -e "require test/ttester.fs" $src -e "#errors @ [if] abort [else] bye [then]"
touch $out
      '';
    };
    pforth = {
      ll = mkDerivation {
        name = "theForthNet-ll-tests-pforth";
        src = ./tests;
        buildInputs = [ pkgs.pforth pkgs.theForthNet.ttester pkgs.theForthNet.ll ];
        copyForthPackagesToBuild = true;
        buildPhase = ''
runHook preBuild
set -e

# kinda janky actual buildPhase
set -e
cat > _tmp.fs <<EOF
include forth-packages/ttester/x.x.x/ttester.4th
include forth-packages/ll/x.x.x/ll.fs
include ll.fs
#errors @ [if] abort [then]
EOF
pforth _tmp.fs
touch $out
runHook postBuild
'';
      };
    };
  };
}
