addGforthPath() {
	gforthSitePackages=@sitePackages@
    addToSearchPathWithCustomDelimiter : GFORTHPATH $1/@sitePackages@
	addToSearchPathWithCustomDelimiter : GFORTHPATH "$1/${gforthSitePackages/share/lib}"
}

addEnvHooks "$hostOffset" addGforthPath
